import React from "react";
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import Home from './Home';
import Map from './Map';
import RestaurantOverzicht from './RestaurantOverzicht';
import RestaurantDetail from './RestaurantDetail';

var home = () => <Home/>;
var map = () => <Map/>;
var restaurantoverzicht = () => <RestaurantOverzicht/>;
var restaurantdetail = () => <RestaurantDetail/>;
const Navigation = () => (
    <Router>
        <div>
            <nav id={'navigation'}>
                <ul>
                    <li>
                        <Link to="/index">Home</Link>
                    </li>
                    <li>
                        <Link to="/map">Map</Link>
                    </li>
                    <li>
                        <Link to="/restaurantoverzicht">Overzicht</Link>
                    </li>
                </ul>
            </nav>


            <Route path="/index"  component={home}/>
            <Route path="/map" component={map}/>
            <Route path="/restaurantoverzicht" component={restaurantoverzicht}/>
            <Route path='/restaurantdetail/:id' component={restaurantdetail}/>
        </div>
    </Router>
);

export default Navigation;