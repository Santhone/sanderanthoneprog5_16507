import React from 'react';
import L from 'leaflet';
import 'leaflet-routing-machine';
// eslint-disable-next-line
import { Link } from 'react-router-dom';
import RestaurantList from './RestaurantList';
import data from '../data/restaurants.json';

class Map extends React.Component {
    componentDidMount() {

        const map = L.map('map', {
            center: [51.019332, 4.398900],
            zoom: 9,
            layers: [
                L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {}),
            ]
        });

        var control = L.Routing.control({
            waypoints: [
                L.latLng(51.019332, 4.398900),
                L.latLng(51.019332, 3.398900)
            ],
            routeWhileDragging: true,
        }).addTo(map);

        function createButton(label, container) {
            var btn = L.DomUtil.create('button', '', container);
            btn.setAttribute('type', 'button');
            btn.innerHTML = label;
            return btn;
        }


        var restaurants;

        const GetRestaurants = async() => {
            restaurants = data.restaurant;
            console.log(restaurants);


            // eslint-disable-next-line
            restaurants.map((restaurant => {
                var container = L.DomUtil.create('div'),
                title = L.DomUtil.create('h1','',container),
                    startBtn = createButton('Start from this location', container),
                    destBtn = createButton('Go to this location', container);
                title.setAttribute('className', 'restotitle');
                title.innerHTML = `${restaurant.name}`;
                L.marker([restaurant.latitude, restaurant.longitude]).addTo(map)
                    .bindPopup(container).openPopup();

                L.DomEvent.on(startBtn, 'click', function() {
                    control.spliceWaypoints(0, 1, [restaurant.latitude, restaurant.longitude]);
                    map.closePopup();
                });
                L.DomEvent.on(destBtn, 'click', function() {
                    control.spliceWaypoints(control.getWaypoints().length - 1, 1, [restaurant.latitude, restaurant.longitude]);
                    map.closePopup();
                });
            }));

            map.on('click', function(e) {
                var container = L.DomUtil.create('div'),
                    startBtn = createButton('Start from this location', container),
                    destBtn = createButton('Go to this location', container);

                L.popup()
                    .setContent(container)
                    .setLatLng(e.latlng)
                    .openOn(map);

                L.DomEvent.on(startBtn, 'click', function() {
                    control.spliceWaypoints(0, 1, e.latlng);
                    map.closePopup();
                });
                L.DomEvent.on(destBtn, 'click', function() {
                    control.spliceWaypoints(control.getWaypoints().length - 1, 1, e.latlng);
                    map.closePopup();
                });
            });
            var elements = document.getElementsByClassName('restaurant');
            console.log(elements);
            for (let i = 0; i < elements.length; i++) {
                console.log("hello " + restaurants[i]);
                // eslint-disable-next-line
                elements.item(i).addEventListener('click', function() {
                    map.setView([restaurants[i].latitude, restaurants[i].longitude], 11);
                });
            }
            var mapresetter = document.getElementsByClassName('resetmap')[0];
            mapresetter.addEventListener('click', function() { map.setView([51.019332, 4.398900], 9) })
        };

        GetRestaurants();



    }

    render() {
        return <div id="mapwrapper">
        <div id="map" 
        className={"mapelement"}
        style={
        {width: "80%", 
        height: "600px", 
        float: "center", 
        padding: "1em",
        border: "solid"}
        }>
        </div>
        <RestaurantList/>
        </div>
    }
}

export default Map;
