import React, { Component } from 'react';
import data from '../data/restaurants.json';

class RestaurantDetail extends Component {

 render() {
  var currenturl = window.location.href;
  var urlstring = `${currenturl}`;
  var final = urlstring.substr(urlstring.lastIndexOf('/') + 1) - 1;
  var detailrestaurant = data.restaurant[final];

  return (
   <div id="detaildiv">
     <h1> {detailrestaurant.name} </h1>
     <img id="detailimage" src={`/images/${detailrestaurant.image}`} alt="restofoto"></img>
     <table id="detailtable">
       <tr>
        <td> Aantal sterren </td>
        <td> {detailrestaurant.sterren}</td>
       </tr>
       <tr>
        <td> Chef </td>
        <td> {detailrestaurant.chef}</td>
       </tr>
       <tr>
        <td> Gevestigd in </td>
        <td> {detailrestaurant.gemeente}, {detailrestaurant.provincie}</td>
       </tr>
         <tr>
        <td> Recensie </td>
        <td> {detailrestaurant.comment}</td>
       </tr>
     </table>
</div>
  );
 }
}

export default RestaurantDetail;
