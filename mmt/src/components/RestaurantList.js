import React from 'react';
import data from '../data/restaurants.json';

class RestaurantList extends React.Component {


    state = {
        restaurants: data.restaurant,
    };

    render() {
        const {restaurants} = this.state;
        console.log(this.state);
        return (
            <div className={"mapelement"}
                style={
                {width: "15%", 
                 height: "auto", 
                float: "right"}
                }>
                <h1 id="name"> Restaurants</h1>
                <div id="restaurant">{restaurants.map((restaurant => {
                    return(<p className="restaurant">{restaurant.name}</p>)
                }))}
                </div>
                <div className="resetmap"> Reset Map
                </div>
            </div>
        );
    }
}

export default RestaurantList