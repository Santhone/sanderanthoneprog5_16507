import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import data from '../data/restaurants.json';

class RestaurantOverzicht extends Component {

    list = data.restaurant;


    render() {
        return (
            <table>
               {this.list.map(item => {
                return (
            <tr>
                <td style={{padding: "1em"}}><b>{item.name}</b></td>
                <td>
                 <Link to={`/restaurantdetail/${item.key}`}>Overzicht</Link>
                </td>
            </tr>
        );
    })}
            </table>
        );
    }
}

export default RestaurantOverzicht;
