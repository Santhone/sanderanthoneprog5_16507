import React, {Component} from 'react';

class Home extends Component {
    render() {
        return (
            <div id="Homepage">
                <h1 id="homepagetitle">Sterrenrestaurants in België</h1>
                <img id="michelinlogo" src="/images/michelinlogo.jpg" alt="logo michelin"></img>
            </div>
        );
    }
}

export default Home;