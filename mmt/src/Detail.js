import React, {Component} from 'react';

class Detail extends Component {
    render() {
        return (
            <main>
                <h1>{this.props.data.name}</h1>
               <td><button onClick={() => this.props.action('List')}>List</button></td>
             </main>
        )
    }
}

export default Detail; 