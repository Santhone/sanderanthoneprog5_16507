// Define map
const myMap = L.map('map');

// Define icon
const treePng = L.icon({
    iconUrl: 'https://www.freeiconspng.com/uploads/mango-tree-876-900--textures--entourage--pinterest-15.png',
    shadowUrl: '',
    iconSize: [35, 65]
});

// Define basemap
const myBasemap = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});

// Add basemap to map id
myBasemap.addTo(myMap);

// Set center of the map
myMap.setView([51.1836426, 4.3575222], 12);

// Make an XMLHttpRequest to the JSON data
const request = new XMLHttpRequest();
request.open('GET', 'map.json', true);

request.onload = function () {
    // begin accessing JSON data here
    const data = JSON.parse(this.response);

    // Reduce neighborhoods down to how many they are, and count them
    const neighborhoodCount = data.cafes.reduce((sums, cafe) => {
        sums[cafe.neighborhood] = (sums[cafe.neighborhood] || 0) + 1;
        return sums;
    }, {});

    // Create a sidebar
    const sidebar = document.getElementById('neighborhoods');
    const h3 = document.createElement("h3");
    h3.innerHTML = "Buurtopdeling";
    sidebar.appendChild(h3);

    // Print all neighborhoods in sidebar
    for (let neighborhood in neighborhoodCount) {
        const p = document.createElement("p");
        p.innerHTML = `<b>${neighborhood}</b> : ${neighborhoodCount[neighborhood]}`;
        sidebar.appendChild(p);
    }

    // Print cafe markers
    const cafes = data.cafes.map(cafe => {
        console.log(cafe.name);

        L.marker([cafe.lat, cafe.long], {
            icon: treePng
        }).bindPopup(`
        <h2>${cafe.name}</h2>
        <p><b>Buurt:</b> ${cafe.neighborhood}</p>
        <p><b>Opmerkingen:</b> ${cafe.comments}</p>
    `).openPopup().addTo(myMap);
    });
}

request.send();
