var scrollDelay = 2000;
var marqueeSpeed = 1;
var timer;

var scrollArea;
var marquee;
var scrollPosition = 0;
var versnellen;
var vertragen;


var scrolling = function () {
    if (scrollPosition + scrollArea.offsetHeight <= 0) {
        scrollPosition = marquee.offsetHeight;

    } else {
        scrollPosition = scrollPosition - marqueeSpeed;
    }
    scrollArea.style.top = scrollPosition + "px";
}

var startScrolling = function () {
    timer = setInterval(scrolling, 30);
}

var initializeMarquee = function () {
    scrollArea = document.getElementById("scroll-area");
    scrollArea.style.top = 0;
    marquee = document.getElementById("marquee");
    versnellen = document.getElementById("versnellen");
    vertragen = document.getElementById("vertragen");
    setTimeout(startScrolling, scrollDelay);
}

var pauseMarquee = function () {
    if (marqueeSpeed > 0) {
        marqueeSpeed = 0;
    }
    else {
        marqueeSpeed = 1;
    }
}

var speedUpMarquee = function () {
    if (marqueeSpeed <= 3) {
        marqueeSpeed++;
    }

}

var speedDownMarquee = function () {
    if (marqueeSpeed >= 0) {
        marqueeSpeed--;
    }

}

window.onload = initializeMarquee();
marquee.addEventListener("mouseover", pauseMarquee);
marquee.addEventListener("mouseout", pauseMarquee);
versnellen.addEventListener("click",speedUpMarquee);
vertragen.addEventListener("click",speedDownMarquee);