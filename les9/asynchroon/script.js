        function final() {
            
            clearPage();
            
            if (document.getElementById('zoekterm').value == "") {
                var div = document.createElement('div');
                div.innerHTML = "Gelieve een zoekwaarde in te geven";
                div.style.fontSize = '20';
                div.style.fontWeight = 'bold';
                document.getElementById("root").appendChild(div);
            }
            else {
                setQueryParameters()
            }
        }

        function setQueryParameters() {


            if (document.getElementById('10results').checked) {
                var amountofresults = document.getElementById('10results').value;
            }
            if (document.getElementById('20results').checked) {
                var amountofresults = document.getElementById('20results').value;
            }
            if (document.getElementById('50results').checked) {
                var amountofresults = document.getElementById('50results').value;
            }
            var alfa = document.getElementById('zoekterm').value;
            var beta = amountofresults;
            loadHtmlPage(alfa, beta)
        }

        function clearPage() {
            var elem = document.getElementById("root");
            if (elem.hasChildNodes()) {
                elem.remove();
                var rootdiv = document.createElement('div');
                rootdiv.id = "root";
                rootdiv.className = "grid-container"
                document.querySelector("body").appendChild(rootdiv);
            }
        }

        function loadHtmlPage(SearchValue, NumberOfResults) {

            clearPage();
            function FillPage(data) {
                const char = data.data.results.map(char => {
                    var figure = document.createElement('figure');
                    figure.id = "char";
                    figure.className = "grid-item";
                    var image = document.createElement('img');
                    image.className = "char-img";
                    image.style.width = '80%';
                    image.src = char.thumbnail.path + "." + char.thumbnail.extension;
                    var figcaption = document.createElement('figcaption')
                    figcaption.className = "char-name"
                    figcaption.innerHTML = char.name;
                    figure.appendChild(figcaption)
                    figure.appendChild(image);
                    document.getElementById("root").appendChild(figure);
                })
            };

            // ` this is a backtick
            // Replace ./data.json with your JSON feed
            fetch(`https://gateway.marvel.com:443/v1/public/characters?nameStartsWith=${SearchValue}&limit=${NumberOfResults}&apikey=0b93dcc403314300ba421e6adf998dfe`, { method: 'GET' })
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    FillPage(data);
                })
                .catch(err => {
                    // Do something for an error here
                    var div = document.createElement('div');
                    div.innerHTML = err.stack;
                    document.getElementById('root').appendChild(div);
                });
        };
        